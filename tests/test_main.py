# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
import zsnl_amqp_consumers.__main__
from unittest import mock
from zsnl_domains import case_management


class TestMain(unittest.TestCase):
    """Test the __main__ module"""

    @mock.patch("zsnl_amqp_consumers.__main__.fileConfig")
    @mock.patch("zsnl_amqp_consumers.__main__.InfrastructureFactory")
    @mock.patch("zsnl_amqp_consumers.__main__.CQRS")
    @mock.patch("zsnl_amqp_consumers.__main__.AMQPClient")
    def test_main(
        self, mock_amqp, mock_cqrs, mock_infrafactory, mock_fileconfig
    ):
        """Main function"""
        # zsnl_amqp_consumers.__main__
        zsnl_amqp_consumers.__main__.main()

        mock_fileconfig.assert_called_once_with("logging.conf")
        mock_infrafactory.assert_called_once_with(config_file="config.conf")
        mock_cqrs.assert_called_once_with(
            domains=[case_management],
            infrastructure_factory=mock_infrafactory(),
            command_wrapper_middleware=[],
        )

        mock_amqp.assert_called_with(
            mock_cqrs().infrastructure_factory.get_config(), cqrs=mock_cqrs()
        )
