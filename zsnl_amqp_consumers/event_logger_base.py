# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .repository import UserInformation
from abc import ABCMeta, abstractmethod
from minty import Base
from zsnl_domains.database.schema import Logging


class BaseLogger(Base, metaclass=ABCMeta):
    @abstractmethod
    def __call__(self):
        pass

    def _create_logging_record(
        self,
        user_info: UserInformation,
        event_type: str,
        subject: str,
        component: str,
        component_id: int,
        case_id: int,
        created_date: str,
        event_data: dict,
        created_for=None,
        restricted=False,
    ) -> Logging:
        """Create `Logging` record from event data.

        :param user_info: information about user
        :type user_info: UserInformation
        :param event_type: event type
        :type event_type: str
        :param subject: subject logline
        :type subject: str
        :param component: type of component
        :type component: str
        :param component_id: component id
        :type component_id: int
        :param case_id: case id
        :type case_id: int
        :param created_date: created date
        :type created_date: str
        :param event_data: event_data
        :type event_data: dict
        :return: logging record
        :rtype: Logging"""
        record = Logging()

        created_by = f"betrokkene-{user_info.type}-{user_info.id}"

        record.zaak_id = case_id
        record.component = component
        record.component_id = component_id
        record.onderwerp = subject
        record.created = created_date
        record.last_modified = created_date
        record.event_type = event_type
        record.event_data = event_data
        record.created_by = created_by
        record.created_by_name_cache = f"{user_info.display_name}"
        record.restricted = restricted
        record.created_for = created_for

        return record

    def format_entity_data(self, event) -> dict:
        """Create formatted dict from event.changes & event.entity_data.

        :param event: event
        :type event: event
        :return: dict with entity_data
        :rtype: dict
        """
        formatted_event_data = dict(event["entity_data"])
        for change in event["changes"]:
            formatted_event_data[change["key"]] = change["new_value"]
            old_key = f"{change['key']}_old"
            formatted_event_data[old_key] = change["old_value"]
        return formatted_event_data
