# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    CaseInformation,
    Contact,
    FileInformation,
    UserInformation,
    get_case,
    get_case_from_thread,
    get_contact_from_thread,
    get_file,
    get_user,
)
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Logging


class CommunicationBase(BaseLogger):
    variables = None
    subject = None

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """

        # For incoming email, the event["user_uuid"] is "None"
        if event["user_uuid"] in ["None", None]:
            user_info = UserInformation(None, None, None)
        else:
            try:
                user_info = get_user(session=session, uuid=event["user_uuid"])
            except NotFound:
                return

        formatted_entity_data = self.format_entity_data(event=event)

        recipient = get_contact_from_thread(
            session=session,
            thread_uuid=(
                formatted_entity_data.get("thread_uuid", None)
                or formatted_entity_data.get("uuid", None)
            ),
        )

        if (
            "case_uuid" in formatted_entity_data
            and formatted_entity_data["case_uuid"] is not None
        ):
            self.logger.debug(
                f"Retrieving case by UUID: {formatted_entity_data['case_uuid']}"
            )
            case = get_case(
                session=session, uuid=formatted_entity_data["case_uuid"]
            )
        else:
            self.logger.debug("Retrieving case by thread")
            case = get_case_from_thread(
                session=session,
                thread_uuid=(
                    formatted_entity_data.get("thread_uuid", None)
                    or formatted_entity_data.get("uuid", None)
                ),
            )

        if case:
            self.logger.debug(f"Retrieved case {case.id}")

        file_info = None
        if formatted_entity_data.get(
            "is_imported"
        ) and formatted_entity_data.get("original_message_file"):
            file_info = get_file(
                session=session,
                uuid=(formatted_entity_data["original_message_file"]),
            )

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            recipient=recipient,
            case=case,
            user_info=user_info,
            file_info=file_info,
        )
        attributes = self.generate_component_attributes(event_parameters)

        if case.id:
            return self._create_logging_record(
                user_info=user_info,
                event_type=attributes["event_type"],
                subject=self._generate_subject(event_parameters),
                component=attributes["component"],
                component_id=None,
                case_id=case.id if case else None,
                created_date=event["created_date"],
                event_data=event_parameters,
                created_for=event_parameters.get("subject_id", None),
                restricted=(case.confidentiality != "public"),
            )
        else:
            self.logger.debug("Not creating log record")
            return

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """

        if self.variables:
            variables = [
                event_parameters[variable] for variable in self.variables
            ]
            subject = self.subject.format(*variables)
            return subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        recipient: Contact = None,
        case: CaseInformation = None,
        user_info: UserInformation = None,
        file_info: FileInformation = None,
    ) -> dict:
        raise NotImplementedError

    def generate_component_attributes(self, event_parameters=None) -> dict:
        raise NotImplementedError


class ThreadToCaseLinked(CommunicationBase):
    subject = "Bericht toegevoegd aan zaak '{}'"
    variables = ["case_id"]

    def generate_component_attributes(self, event_parameters=None):
        return {"component": "zaak", "event_type": "case/thread/link"}

    def generate_event_parameters(
        self,
        entity_data: dict,
        recipient: Contact = None,
        case: CaseInformation = None,
        user_info: UserInformation = None,
        file_info: FileInformation = None,
    ):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :param recipient: Contact named tuple with contact info
        :type recipient: Contact
        :param case: CaseInformation named tuple with case info
        :type case: CaseInformation
        :type user_info: UserInformation
        :type file_info: FileInformation
        :return: event parameters
        :return: event parameters
        :rtype: dict
        """

        return {"case_id": case.id}


class MessageDeleted(CommunicationBase):
    subject = "{} heeft bericht van type {} verwijderd."
    variables = ["username", "message_type"]

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate specific subject for MessageDeleted event.

        :param event_parameters: Generated event parameters from entity data.
        :return: string with the formatted subject
        """
        translated_type = {
            "note": "notitie",
            "contact_moment": "contactmoment",
        }
        if event_parameters["message_type"] in translated_type:
            event_parameters["message_type"] = translated_type[
                event_parameters["message_type"]
            ]
            self.subject = "{} heeft {} verwijderd."

        variables = [event_parameters[variable] for variable in self.variables]
        return self.subject.format(*variables)

    def generate_component_attributes(self, event_parameters=None) -> dict:
        return {
            "component": "betrokkene",
            "event_type": "subject/message/delete",
        }

    def generate_event_parameters(
        self,
        entity_data: dict,
        recipient: Contact = None,
        case: CaseInformation = None,
        user_info: UserInformation = None,
        file_info: FileInformation = None,
    ) -> dict:
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :param recipient: Contact named tuple with contact info
        :type recipient: Contact
        :param case: CaseInformation named tuple with case info
        :type case: CaseInformation
        :param case: UserInformation named tuple with user info
        :type user_info: UserInformation
        :type file_info: FileInformation
        :return: event parameters
        :rtype: dict
        """

        message_type = entity_data.get("message_type", None)
        if "external_message_type" in entity_data:
            self.logger.debug(
                f"External message type: {entity_data['external_message_type']}"
            )
            message_type = entity_data["external_message_type"]

        event_parameters = {
            "case_id": case.id,
            "username": user_info.display_name,
            "message_type": message_type,
        }

        self.logger.info("Event parameters: " + str(event_parameters))

        return event_parameters


class ExternalMessageCreated(CommunicationBase):
    subject = "{} toegevoegd"
    variables = ["message_type"]

    def generate_component_attributes(self, event_parameters=None):
        event_type = "case/pip/feedback"
        if event_parameters["message_type"] == "E-mail":
            event_type = "case/email/created"
        return {"component": "zaak", "event_type": event_type}

    def generate_event_parameters(
        self,
        entity_data: dict,
        recipient: Contact = None,
        case: CaseInformation = None,
        user_info: UserInformation = None,
        file_info: FileInformation = None,
    ) -> dict:
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :param recipient: Contact named tuple with contact info
        :type recipient: Contact
        :param case: CaseInformation named tuple with case info
        :type case: CaseInformation
        :param case: UserInformation named tuple with user info
        :type user_info: UserInformation
        :type file_info: FileInformation
        :return: event parameters
        :rtype: dict
        """
        translated_type = {"pip": "PIP-bericht", "email": "E-mail"}
        external_message_type = entity_data.get("external_message_type", None)
        translated_message_type = translated_type.get(external_message_type)
        self.subject = "{} toegevoegd"
        self.variables = ["message_type"]

        event_parameters = {
            "case_id": case.id,
            "content": "",
            "message_type": translated_message_type,
        }

        # If the email_message is imported, use different logging message
        if file_info and entity_data.get("is_imported"):
            event_parameters.update({"filename": file_info.name})
            self.subject = "{} '{}' als bericht geïmporteerd."
            self.variables.append("filename")

        return event_parameters


class NoteCreated(CommunicationBase):
    subject = "{} toegevoegd"
    variables = ["message_type"]

    def generate_component_attributes(self, event_parameters=None):
        return {"component": "zaak", "event_type": "case/note/created"}

    def generate_event_parameters(
        self,
        entity_data: dict,
        recipient: Contact = None,
        case: CaseInformation = None,
        user_info: UserInformation = None,
        file_info: FileInformation = None,
    ) -> dict:
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :param recipient: Contact named tuple with contact info
        :type recipient: Contact
        :param case: CaseInformation named tuple with case info
        :type case: CaseInformation
        :param case: UserInformation named tuple with user info
        :type user_info: UserInformation
        :type file_info: FileInformation
        :return: event parameters
        :rtype: dict
        """

        event_parameters = {
            "case_id": case.id,
            "content": "",
            "message_type": "Notitie",
        }

        return event_parameters


class ContactMomentCreated(CommunicationBase):
    subject = "{} toegevoegd"
    variables = ["message_type"]

    def generate_component_attributes(self, event_parameters=None):
        return {
            "component": "zaak",
            "event_type": "case/contact_moment/created",
        }

    def generate_event_parameters(
        self,
        entity_data: dict,
        recipient: Contact = None,
        case: CaseInformation = None,
        user_info: UserInformation = None,
        file_info: FileInformation = None,
    ) -> dict:
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :param recipient: Contact named tuple with contact info
        :type recipient: Contact
        :param case: CaseInformation named tuple with case info
        :type case: CaseInformation
        :param case: UserInformation named tuple with user info
        :type user_info: UserInformation
        :type file_info: FileInformation
        :return: event parameters
        :rtype: dict
        """

        event_parameters = {
            "case_id": case.id,
            "content": "",
            "message_type": "Contactmoment",
        }

        return event_parameters
